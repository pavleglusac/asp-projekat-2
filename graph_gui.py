import networkx as nx
import matplotlib.pyplot as plt
from build import get_graph_and_trie
from pyvis import network as net

graph, trie = get_graph_and_trie()
edges = graph.edges()
G = nx.DiGraph()
for edge in edges:
    u, v = edge.endpoints()
    print(u.get_short_name(), end=" ")
    print(v.get_short_name())
    G.add_edge(u.get_short_name(), v.get_short_name())


# nx.draw_networkx(G, arrows=True)
# plt.show()


def draw_graph3(networkx_graph, notebook=True, output_filename='graph.html', show_buttons=True,
                only_physics_buttons=True):
    pyvis_graph = net.Network(notebook=notebook)
    pyvis_graph.width = '1920px'
    pyvis_graph.height = '1080px'
    for node, node_attrs in networkx_graph.nodes(data=True):
        pyvis_graph.add_node(node, **node_attrs)

    for source, target, edge_attrs in networkx_graph.edges(data=True):
        if not 'value' in edge_attrs and not 'width' in edge_attrs and 'weight' in edge_attrs:
            edge_attrs['value'] = edge_attrs['weight']
        pyvis_graph.add_edge(source, target, **edge_attrs)

    if show_buttons:
        if only_physics_buttons:
            pyvis_graph.show_buttons(filter_=['physics'])
        else:
            pyvis_graph.show_buttons()

    return pyvis_graph.show(output_filename)


draw_graph3(G, output_filename='graph_output.html', notebook=False)
