class Graph:
    """ Reprezentacija jednostavnog grafa"""

    # ------------------------- Ugnježdena klasa Edge -------------------------
    class Edge:
        """ Struktura koja predstavlja ivicu grafa """
        __slots__ = '_origin', '_destination', '_element'

        def __init__(self, origin, destination, element):
            self._origin = origin
            self._destination = destination
            self._element = element

        def endpoints(self):
            """ Vraća torku (u,v) za čvorove u i v."""
            return (self._origin, self._destination)

        def opposite(self, v):
            """ Vraća čvor koji se nalazi sa druge strane čvora v ove ivice."""
            if not isinstance(v, Graph.Vertex):
                raise TypeError('v mora biti instanca klase Vertex')
            if self._destination == v:
                return self._origin
            elif self._origin == v:
                return self._destination
            raise ValueError('v nije čvor ivice')

        def element(self):
            """ Vraća element vezan za ivicu"""
            return self._element

        def __hash__(self):  # omogućava da Edge bude ključ mape
            return hash((self._origin, self._destination))

        def __str__(self):
            return '({0},{1},{2})'.format(self._origin, self._destination, self._element)

    # ------------------------- Metode klase Graph -------------------------
    def __init__(self, directed=False):
        """ Kreira prazan graf (podrazumevana vrednost je da je neusmeren).

        Ukoliko se opcioni parametar directed postavi na True, kreira se usmereni graf.
        """
        self._outgoing = {}
        # ukoliko je graf usmeren, kreira se pomoćna mapa
        self._incoming = {} if directed else self._outgoing

        self.nameVertex = {}

    def _validate_vertex(self, v):
        """ Proverava da li je v čvor(Vertex) ovog grafa."""
        if not isinstance(v, Vertex):
            raise TypeError('Očekivan je objekat klase Vertex')
        if v not in self._outgoing:
            raise ValueError('Vertex ne pripada ovom grafu.')

    def is_directed(self):
        """ Vraća True ako je graf usmeren; False ako je neusmeren."""
        return self._incoming is not self._outgoing  # graf je usmeren ako se mape razlikuju

    def vertex_count(self):
        """ Vraća broj čvorova u grafu."""
        return len(self._outgoing)

    def vertices(self):
        """ Vraća iterator nad svim čvorovima grafa."""
        return self._outgoing.keys()

    def edge_count(self):
        """ Vraća broj ivica u grafu."""
        total = sum(len(self._outgoing[v]) for v in self._outgoing)
        # ukoliko je graf neusmeren, vodimo računa da ne brojimo čvorove više puta
        return total if self.is_directed() else total // 2

    def edges(self):
        """ Vraća set svih ivica u grafu."""
        result = set()  # pomoću seta osiguravamo da čvorove neusmerenog grafa brojimo samo jednom
        for secondary_map in self._outgoing.values():
            result.update(secondary_map.values())  # dodavanje ivice u rezultujući set
        return result

    def get_edge(self, u, v):
        """ Vraća ivicu između čvorova u i v ili None ako nisu susedni."""
        self._validate_vertex(u)
        self._validate_vertex(v)
        return self._outgoing[u].get(v)

    def degree(self, v, outgoing=True):
        """ Vraća stepen čvora - broj(odlaznih) ivica iz čvora v u grafu.

        Ako je graf usmeren, opcioni parametar outgoing se koristi za brojanje dolaznih ivica.
        """
        self._validate_vertex(v)
        adj = self._outgoing if outgoing else self._incoming
        return len(adj[v])

    def incident_edges(self, v, outgoing=True):
        """ Vraća sve (odlazne) ivice iz čvora v u grafu.

        Ako je graf usmeren, opcioni parametar outgoing se koristi za brojanje dolaznih ivica.
        """
        self._validate_vertex(v)
        adj = self._outgoing if outgoing else self._incoming
        for edge in adj[v].values():
            yield edge

    def insert_vertex(self, x):
        """ Ubacuje i vraća novi čvor (Vertex)"""
        v = x
        self._outgoing[v] = {}
        if self.is_directed():
            self._incoming[v] = {}  # mapa različitih vrednosti za dolazne čvorove
        self.nameVertex[v.name] = v
        return v

    def insert_edge(self, u, v, x=None):
        """ Ubacuje i vraća novu ivicu (Edge) od u do v sa pomoćnim elementom x.

        Baca ValueError ako u i v nisu čvorovi grafa.
        Baca ValueError ako su u i v već povezani.
        """
        if self.get_edge(u, v) is not None:  # uključuje i proveru greške
            raise ValueError('u and v are already adjacent')
        e = self.Edge(u, v, x)
        self._outgoing[u][v] = e
        self._incoming[v][u] = e

    def links_to_edges(self):
        for vertex in self.vertices():
            for link in vertex.links:
                self.insert_edge(vertex, self.get_vertex_by_name(link.replace("\\", "/")))

    def get_vertex_by_name(self, name):
        return self.nameVertex[name]

    def get_incoming(self, u):
        edges = self._incoming[u]
        vertices = edges
        return vertices


RED = "\033[1;31m"
BLUE = "\033[1;34m"
CYAN = "\033[1;36m"
GREEN = "\033[0;32m"
RESET = "\033[0;0m"


class Vertex:
    """ Struktura koja predstavlja čvor grafa."""
    __slots__ = '_element', 'trie', 'name', 'links', 'words'

    def __init__(self, trie, name, words, links):
        self.trie = trie
        self.name = name
        self.links = links
        self.words = words
        self._element = name

    def element(self):
        """Vraća element vezan za čvor grafa."""
        return self._element

    def __hash__(self):  # omogućava da Vertex bude ključ mape
        return hash(id(self))

    def __str__(self):
        return str(self.name)

    def get_short_name(self):
        return self.name.split("/")[len(self.name.split("/")) - 1]

    def get_description(self, query, index):
        i = index - 5
        j = index + 5
        if i < 0:
            i = 0
        if j >= len(self.words):
            j = len(self.words) - 1
        s = ""
        for r in range(i, j + 1):
            if r == index or self.words[r].lower() == query:
                s += RED + self.words[r] + RESET + " "
            else:
                s += self.words[r] + " "
        return s

    def get_descriptions(self, query, indexes, left = 5, right = 5):
        for index in indexes:
            i = index - left
            j = index + right
            if i < 0:
                i = 0
            if j >= len(self.words):
                j = len(self.words) - 1
            s = ""
            for r in range(i, j + 1):
                if r == index or self.words[r].lower() == query:
                    s += RED + self.words[r] + RESET + " "
                else:
                    s += self.words[r] + " "
            return s

    def get_phrase_description(self, query, indexes, left = 5, right = 5):
            index = indexes
            i = index - left
            j = index + right
            if i < 0:
                i = 0
            if j >= len(self.words):
                j = len(self.words) - 1
            s = ""
            for r in range(i, j + 1):
                if r == index or r in query:
                    s += RED + self.words[r] + RESET + " "
                else:
                    s += self.words[r] + " "
            return s