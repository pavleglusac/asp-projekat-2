import speech_recognition as sr

def listen():
    recognizer = sr.Recognizer()
    microphone = sr.Microphone()

    with microphone as source:
        print("Slušam . . .")
        recognizer.adjust_for_ambient_noise(source)
        audio = recognizer.listen(source)

    response = {
        "success": True,
        "error": None,
        "transcription": None
    }

    try:
        response["transcription"] = recognizer.recognize_google(audio)
    except sr.RequestError:
        response["success"] = False
        response["error"] = "API unavailable"
    except sr.UnknownValueError:
        response["error"] = "Unable to recognize speech"

    return response

def speech_parser():
    transcription = listen()["transcription"]
    transcription = transcription.replace("and", "AND")
    transcription = transcription.replace("or", "OR")
    transcription = transcription.replace("not", "NOT")
    print(transcription)
    return transcription