class TrieNode:
    def __init__(self, char):
        self.char = char
        self.children = []
        self.word_finished = False
        self.file_index = {}
        self.counter = 1

    def __str__(self):
        return self.char

class Trie:
    def __init__(self, root = None):
        self.root = root

    def add(self, word, file, index):
        node = self.root
        for char in word:
            found_in_child = False
            for child in node.children:
                if child.char == char:
                    child.counter += 1
                    node = child
                    found_in_child = True
                    break
            if not found_in_child:
                new_node = TrieNode(char)
                node.children.append(new_node)
                node = new_node
        node.word_finished = True
        if file in node.file_index:
            node.file_index[file].append(index)
        else:
            node.file_index[file] = [index]

    def find(self, word, prefix = False):
        node = self.root
        for i in word:
            found = False
            for child in node.children:
                if child.char == i:
                    node = child
                    found = True
                    break
            if not found:
                return False, None
        if prefix:
            # get index of finished words
            return True, None

        if not node.word_finished:
            return False, None

        return True, node.file_index

    def find_prefix(self, prefix):
        node = self.root
        if not self.root.children:
            return False, 0
        for char in prefix:
            char_not_found = True
            for child in node.children:
                if child.char == char:
                    char_not_found = False
                    node = child
                    break
            if char_not_found:
                return False, 0
        return True, node.counter

    def find_possible_words(self, word):
        node = self.root
        for ch in word:
            for child in node.children:
                if child.char == ch:
                    node = child
                    break
        setic = set({})
        if self.find(word)[0]:
            setic.add(word)
        self.get_words(node, word, setic)
        return list(setic)

    def list_words(self):
        my_list = []
        self.print_words(self.root, "")

    def print_words(self, node, stri):
        for i in node.children:
            self.print_words(i, stri + i.char)
        if len(node.children) == 0:
            print(stri)

    def get_words(self, node, stri, setic):
        for i in node.children:
            if i.word_finished:
                setic.add(stri + i.char)
            self.get_words(i, stri + i.char, setic)
        if len(node.children) == 0:
            setic.add(stri)
            return stri

if __name__ == "__main__":
    root = TrieNode('*')
    trie = Trie(root)
    trie.add("hackathon", "", 0)
    trie.add('hack', "", 0)
    trie.add("pavle", "", 0)
    trie.add("pav", "", 0)
    trie.add("patlidzan", "", 0)
    trie.add("pakistan", "", 0)
    trie.add("pavlaka", "", 0)

    #trie.list_words()

    print(trie.find_possible_words("pav"))