from prettytable import PrettyTable, PLAIN_COLUMNS

from autocorrect import autocorrect_suggestions
from build import get_graph_and_trie

RED = "\033[1;31m"
BLUE = "\033[1;34m"
CYAN = "\033[1;36m"
GREEN = "\033[0;32m"
RESET = "\033[0;0m"


returned_flag = False

def return_flag():
    global returned_flag
    returned_flag = False


def search(query):
    global returned_flag
    query = query.lower().split()
    # create path matches | path : [ [(word1, [indexes1])... (word2, [indexes2])], words_matched]
    path_matches = {}
    for index, word in enumerate(query):
        has, file_index = trie.find(word)
        if has:
            for path in file_index.keys():
                if path not in path_matches.keys():
                    path_matches[path] = [[], 0]
                path_matches[path][0].append((word, file_index[path]))
                path_matches[path][1] += 1
    boxes = create_and_sort_boxes(path_matches, query)

    print_results(boxes, path_matches, query)


def print_results(boxes, path_matches, query):
    table = PrettyTable()
    table.set_style(PLAIN_COLUMNS)
    table.field_names = ["Redni broj", "Fajl", "Broj pogodaka", "Isečak"]
    global returned_flag
    i = 0
    broken = False
    for matches, box in enumerate(boxes):
        for item in box:
            vertex = graph.get_vertex_by_name(item)
            s = ""
            for word, indexes in path_matches[item][0]:
                s += vertex.get_descriptions(word, [indexes[0]], 3, 3) + "..."
            i += 1
            table.add_row([i, GREEN + vertex.get_short_name() + RESET, len(query) - matches, s])
            if i % 5 == 0:
                print(table)
                o = input("Učitati još rezultata? Pritisnite enter ")
                table.clear_rows()
                if len(o) == 0:
                    continue
                else:
                    broken = True
                    break
        if broken:
            break
    if i < 5:
        print(table)
        if returned_flag == False:
            query = autocorrect_suggestions(query)
            if query is not None:
                returned_flag = True
                search(query)


def create_and_sort_boxes(path_matches, query):
    # create and sort boxes
    boxes = [[] for x in range(len(query) + 1)]
    for key, value in path_matches.items():
        boxes[value[1]].append(key)
    boxes = boxes[::-1]
    for index, box in enumerate(boxes):
        boxes[index] = sort_box(box, query, path_matches)
    return boxes


def sort_box(box, query, path_matches, random_if_same=False):
    '''Sortira kutije po broju reci'''
    word_box = {}
    sorted_box = []
    for path in box:
        num_of_words = 0
        for word, index_list in path_matches[path][0]:
            num_of_words += len(index_list)
        if num_of_words not in word_box.keys():
            word_box[num_of_words] = []
        word_box[num_of_words].append(path)

    sorted_keys = list(word_box.keys())
    sorted_keys.sort(reverse=True)

    for key in sorted_keys:
        if len(word_box[key]) > 1:
            if not random_if_same:
                sorted_box.extend(sort_links(word_box[key], query, graph, path_matches))
            else:
                sorted_box.extend(word_box[key])
        else:
            sorted_box.extend(word_box[key])
    return sorted_box


def sort_links(box, query, graph, path_matches):
    '''Sortira kutije po broju povezanih linkova'''
    link_box = {}
    incomings = {}
    sorted_box = []

    for path in box:
        vertex = graph.get_vertex_by_name(path)
        incoming = graph.get_incoming(vertex)
        incomings[path] = incoming
        if len(incoming) in link_box.keys():
            link_box[len(incoming)].append(path)
        else:
            link_box[len(incoming)] = [path]

    sorted_keys = list(link_box.keys())
    sorted_keys.sort(reverse=True)
    for key in sorted_keys:
        if len(link_box[key]) > 1:
            sorted_box.extend(sort_mentions_in_links(link_box[key], incomings, query, graph, path_matches))
        else:
            sorted_box.extend(link_box[key])
    return sorted_box


def sort_mentions_in_links(box, incomings, query, graph, path_matches):
    '''Sortira kutije po broju reci u linkovanim stranama'''
    mention_num = {}
    sorted_mentions = []
    for path in box:
        vertex = graph.get_vertex_by_name(path)
        incoming = graph.get_incoming(vertex)
        incomings[path] = incoming
        total = 0
        for incoming in incomings[path]:
            incoming_path = incoming.name
            if incoming_path in path_matches.keys():
                for word_index_list in path_matches[incoming_path][0]:
                    total += len(word_index_list[1])
        if total not in mention_num:
            mention_num[total] = []
        mention_num[total].append(path)


    keys = list(mention_num.keys())
    keys.sort(reverse=True)
    for key in keys:
        sorted_mentions.extend(mention_num[key])
    return sorted_mentions

if __name__ == "basic_search":
    graph, trie = get_graph_and_trie()