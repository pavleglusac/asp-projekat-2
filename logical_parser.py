from prettytable import PrettyTable, PLAIN_COLUMNS
from basic_search import create_and_sort_boxes, print_results
from build import get_graph_and_trie

RED = "\033[1;31m"
BLUE = "\033[1;34m"
CYAN = "\033[1;36m"
GREEN = "\033[0;32m"
RESET = "\033[0;0m"

graph = None
trie = None

priority = {
    0: "NOT",
    1: "AND",
    2: "OR"
}

# | path : [ [(word1, [indexes1])... (word2, [indexes2])], words_matched ]
# | path : [list_of_tuples, words_matched]

def parse_logic(query):
    global path_matches
    path_matches = {}
    query = query.split()
    if not validate(query):
        print("Greška u izrazu!")
        return
    possibles = []
    modified_query = modify_query(query)
    vertices = graph.vertices()
    query2 = query.copy()
    for vertex in vertices:
        list_of_tuples = []
        for priority_key in priority.keys():
            i = 0
            while i < len(query) - 2:
                operator = query[i + 1]
                if operator == priority[priority_key]:
                    result = functions[operator](query[i], query[i + 2], vertex.name, list_of_tuples)
                    query.pop(i)
                    query.pop(i)
                    query.pop(i)
                    query.insert(i, result)
                    i = 0
                    continue
                else:
                    i += 2
        if query[0] == True:
            possibles.append(vertex)

            path_matches[vertex.name] = [list_of_tuples, len(list_of_tuples)]
        query = query2.copy()

    boxes = create_and_sort_boxes(path_matches, modified_query)
    print_results(boxes, path_matches, modified_query)


def evaluate_not(operand1, operand2, path, list_of_tuples):
    result, file_index_1 = trie.find(operand1)

    if result == True:
        path_index_1 = set(file_index_1.keys())
    else:
        return False

    result, file_index_2 = trie.find(operand2)
    if result == True:
        path_index_2 = set(file_index_2.keys())
    else:
        return True

    if path in path_index_1 and path not in path_index_2:
        list_of_tuples.append((operand1, file_index_1[path]))
        return True
    return False


def evaluate_and(operand1, operand2, path, list_of_tuples):

    if isinstance(operand1, bool):
        op1 = operand1
    else:
        result, file_index = trie.find(operand1)
        if result == True:
            path_index_1 = set(file_index.keys())
            if path in path_index_1:
                op1 = True
                list_of_tuples.append((operand1, file_index[path]))
            else:
                op1 = False
        else:
            return False

    if isinstance(operand2, bool):
        op2 = operand2
    else:
        result, file_index = trie.find(operand2)
        if result == True:
            path_index_2 = set(file_index.keys())
            if path in path_index_2:
                op2 = True
                list_of_tuples.append((operand2, file_index[path]))
            else:
                op2 = False
        else:
            return False

    return op1 and op2



def evaluate_or(operand1, operand2, path, list_of_tuples):
    if isinstance(operand1, bool):
        op1 = operand1
    else:
        result, file_index = trie.find(operand1)
        if result == True:
            path_index_1 = set(file_index.keys())
            if path in path_index_1:
                op1 = True
                list_of_tuples.append((operand1, file_index[path]))
            else:
                op1 = False
        else:
            op1 = False

    if isinstance(operand2, bool):
        op2 = operand2
    else:
        result, file_index = trie.find(operand2)
        if result == True:
            path_index_2 = set(file_index.keys())
            if path in path_index_2:
                op2 = True
                list_of_tuples.append((operand2, file_index[path]))
            else:
                op2 = False
        else:
            op2 = False

    return op1 or op2


def validate(query):
    if len(query) < 3:
        return False
    for i in range(len(query) - 1):
        p, q = query[i] in functions.keys(), query[i + 1] in functions.keys()
        if p and q:
            return False
        if not(p or q):
            return False
    return True

def modify_query(query):
    modified_query = []
    modified_query.append(query[0])
    query[0] = query[0].lower()
    for i in range(1, len(query)):
        if query[i] not in functions.keys():
            if query[i - 1] != "NOT":
                modified_query.append(query[i])
            query[i] = query[i].lower()
    return modified_query

if __name__ == "logical_parser":
    functions = {
        "NOT": evaluate_not,
        "AND": evaluate_and,
        "OR": evaluate_or
    }
    path_matches = {}
    graph, trie = get_graph_and_trie()
