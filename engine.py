from autocomplete import autocomplete_parser
from build import get_graph_and_trie
from basic_search import search, return_flag
from logical_parser import parse_logic
from phrases import phrase_parser
from speech import speech_parser
import os

graph = None
trie = None


def main():
    global graph, trie
    graph, trie = get_graph_and_trie()
    while True:
        query = input("Pretraga (^sr za mikrofon): ")
        detect_query_type(query)


def detect_query_type(query):
    if "AND" in query or "OR" in query or "NOT" in query:
        parse_logic(query)
        return
    query = query.lower()
    if "*" in query:
        autocomplete_parser(query)
    elif "\"" in query:
        phrase_parser(query)
    elif "^sr" in query:
        query = speech_parser()
        detect_query_type(query)
    else:
        return_flag()
        search(query)


if __name__ == '__main__':
    main()
