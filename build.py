import os
import threading
import multiprocessing
import pickle

from trie import Trie, TrieNode
from html_parser import Parser
from graph import Graph, Vertex
import time
import sys
import os

graph = None
trie = None
done = False

FOLDER_NAME = "/python-3.8.3-docs-html"

def load_animation():
    load_str = "Učitavanje podataka . . . "
    ls_len = len(load_str)
    animation = "|/-\\"
    anicount = 0
    counttime = 0

    i = 0

    while (not done):
        time.sleep(0.095)
        load_str_list = list(load_str)
        x = ord(load_str_list[i])
        y = 0
        if x != 32 and x != 46:
            if x > 90:
                y = x - 32
            else:
                y = x + 32
            load_str_list[i] = chr(y)
        res = ''
        for j in range(ls_len):
            res = res + load_str_list[j]
        sys.stdout.write("\r" + res + animation[anicount])
        sys.stdout.flush()
        load_str = res
        anicount = (anicount + 1) % 4
        i = (i + 1) % ls_len
        counttime = counttime + 1


def get_file_paths():
    dirname = os.path.dirname(__file__) + FOLDER_NAME
    path = dirname
    filelist = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if not file.endswith(".html"):
                continue
            filelist.append(os.path.join(root, file).replace("\\", "/"))
    return filelist


def build_graph():
    global done, graph, trie
    # proc = threading.Thread(target=load_animation())
    # proc.start()
    graph = Graph(True)
    parser = Parser()
    paths = get_file_paths()
    trie = Trie(TrieNode(''))
    for path in paths:
        links, words = parser.parse(path)
        for index, word in enumerate(words):
            word = word.lower()
            trie.add(word, path, index)
        vertex = Vertex(trie, path, words, links)
        graph.insert_vertex(vertex)
    graph.links_to_edges()
    done = True
    print("Podaci učitani")
    save()

def save():
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, 'data/graph_data')
    outfile = open(filename, 'wb')
    pickle.dump(graph, outfile)
    outfile.close()

    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, 'data/trie_data')
    outfile = open(filename, 'wb')
    pickle.dump(trie, outfile)
    outfile.close()

def load():
    global graph, trie
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, 'data/graph_data')
    infile = open(filename, 'rb')
    graph = pickle.load(infile)
    infile.close()

    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, 'data/trie_data')
    infile = open(filename, 'rb')
    trie = pickle.load(infile)
    infile.close()

def get_graph_and_trie():
    if graph == None:
        load()
        #build_graph()
    return graph, trie


if __name__ == "__main__":
    build_graph()
