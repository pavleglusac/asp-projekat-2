from prettytable import PrettyTable, PLAIN_COLUMNS

from build import get_graph_and_trie
import pdb
graph = None
trie = None

RED = "\033[1;31m"
BLUE = "\033[1;34m"
CYAN = "\033[1;36m"
GREEN = "\033[0;32m"
RESET = "\033[0;0m"


def phrase_parser(query):
    query = query.replace("\"", "").split()
    vertices = graph.vertices()
    n = len(query)
    vertex_boxes = {}
    indexed_query = {}
    for i in range(n):
        if query[i] not in indexed_query.keys():
            indexed_query[query[i]] = []
        indexed_query[query[i]].append(i)

    for vertex in vertices:
        word_indexes = []
        for word in query:
            has, file_index = trie.find(word)
            if has and vertex.name in file_index.keys():
                word_indexes.extend(file_index[vertex.name])
        word_indexes = list(set(word_indexes))
        word_indexes.sort()
        i = 0
        first_index, last_index = -1, -1
        possible = []
        while i < len(word_indexes) - 1:
            while word_indexes[i] + 1 == word_indexes[i + 1]:
                if vertex.words[word_indexes[i]].lower() not in query or vertex.words[word_indexes[i] + 1].lower() not in query:
                    break
                indexes_adjacent = False
                #print(word_indexes[i], vertex.words[word_indexes[i]])
                #print(word_indexes[i + 1], vertex.words[word_indexes[i + 1]])
                for index_A in indexed_query[vertex.words[word_indexes[i]].lower()]:
                    for index_B in indexed_query[vertex.words[word_indexes[i + 1]].lower()]:
                        if index_A - index_B == -1:
                            indexes_adjacent = True
                if not indexes_adjacent:
                    break
                if first_index == -1:
                    first_index, last_index = word_indexes[i], word_indexes[i + 1]
                else:
                    last_index = word_indexes[i + 1]
                i += 1
                if i >= len(word_indexes) - 1:
                    break
            if (first_index, last_index) != (-1, -1):
                possible.append((first_index, last_index))
            first_index, last_index = -1, -1
            i += 1
        maxi = -1
        s_ind = -1
        e_ind = -1
        if len(possible) == 0:
            continue
        for pos in possible:
            if pos[1] - pos[0] > maxi:
                maxi = pos[1] - pos[0]
                s_ind = pos[0]
                e_ind = pos[1]
        if maxi not in vertex_boxes.keys():
            vertex_boxes[maxi] = []
        vertex_boxes[maxi].append((vertex, s_ind))


    x = PrettyTable()
    x.set_style(PLAIN_COLUMNS)
    x.field_names = ["Redni broj", "Fajl", "Broj pogodaka", "Isečak"]
    i = 1
    kys = list(vertex_boxes.keys())
    kys.sort(reverse=True)
    broken = False
    for key in kys:
        for vertex, ind in vertex_boxes[key]:
            x.add_row([i, GREEN + vertex.get_short_name() + RESET, key + 1, vertex.get_phrase_description([x for x in range(ind, ind + key + 1)], ind, 3, n + 2)])
            i += 1
            if i % 5 == 0:
                print(x)
                o = input("Učitati još rezultata? Pritisnite enter ")
                x.clear_rows()
                if len(o) == 0:
                    continue
                else:
                    broken = True
                    break
        if broken:
            break
    if i < 5:
        print(x)

if __name__ == "phrases":
    graph, trie = get_graph_and_trie()
