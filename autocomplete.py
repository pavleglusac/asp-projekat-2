from build import get_graph_and_trie
from prettytable import PrettyTable, PLAIN_COLUMNS

RED = "\033[1;31m"
BLUE = "\033[1;34m"
CYAN = "\033[1;36m"
GREEN = "\033[0;32m"
RESET = "\033[0;0m"

def autocomplete_parser(query):
    query = query.replace("*", "")
    word_list = trie.find_possible_words(query)
    print("Pronađene reči sa zadatim prefiksom: ")
    i = 1
    x = PrettyTable()
    x.set_style(PLAIN_COLUMNS)
    x.field_names = ["Redni broj", "Reč", "Broj pojavljivanja"]
    i = 1
    mention_word = {}
    for word in word_list:
        has, indexes = trie.find(word)
        count = 0
        for file in indexes.keys():
            count += len(indexes[file])
        if count not in mention_word.keys():
            mention_word[count] = []
        mention_word[count].append(word)
    keys = list(mention_word.keys())
    keys.sort(reverse=True)
    broken = False
    for key in keys:
        for item in mention_word[key]:
            x.add_row([i, GREEN + item + RESET, key])
            i += 1
            if i % 6 == 0:
                print(x)
                o = input("Učitati još rezultata? Pritisnite enter ")
                x.clear_rows()
                if len(o) == 0:
                    continue
                else:
                    broken = True
                    break
        if broken:
            break

    if i < 6:
        print(x)

if __name__ == "autocomplete":
    graph, trie = get_graph_and_trie()
