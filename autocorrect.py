from build import get_graph_and_trie

graph = None
trie = None

def autocorrect_suggestions(query):
    if len(query) == 0:
        return
    words = set([])
    trie.get_words(trie.root, "", words)
    suggested_query = []
    for query_word in query:
        ranks = {}
        if trie.find(query_word)[0]:
            suggested_query.append(query_word)
            continue
        for word in words:
            ranks[rank_word(word, query_word)] = word
        keys = list(ranks.keys())
        keys.sort(reverse=True)
        suggested_query.append(ranks[keys[0]])
    if suggested_query == query:
        return
    print("Da li ste mislili ", end = "")
    print(" ".join(suggested_query), "?")
    o = input()
    if o == "da" or o == "Da" or o == "DA":
        return(" ".join(suggested_query))
    return None

def rank_word(word, query_word):
    total_points = 0
    total_points += len(set(word).intersection(set(query_word)))
    total_points += 5*(10 - abs(len(word) - len(query_word)))
    total_points += 5*lcs(word, query_word)
    return total_points


def lcs(word, query_word):
    m = len(word)
    n = len(query_word)
    L = [[0 for x in range(n + 1)] for x in range(m + 1)]

    for i in range(m + 1):
        for j in range(n + 1):
            if i == 0 or j == 0:
                L[i][j] = 0
            elif word[i - 1] == query_word[j - 1]:
                L[i][j] = L[i - 1][j - 1] + 1
            else:
                L[i][j] = max(L[i - 1][j], L[i][j - 1])

    return L[m][n]

if __name__ == "autocorrect":
    graph, trie = get_graph_and_trie()